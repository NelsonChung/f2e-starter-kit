# F2E Starter Kit #

為方便未來處理前端需求與打包，製作快速入門 F2E Starter Kit，可再依需求做局部客製化。

**使用工具**
- [Gulp](https://gulpjs.com/)
- [Compass](http://compass-style.org/)
- [Jade](http://jade-lang.com/)

## 安裝

1. 下載到開發環境
2. 打開 terminal
3. ```cd project_folder ```
4. 執行 ```npm install```
5. 安裝結束後，就可以開始切版了。

## 開發結構

**css**

樣式開發放在 sass 資料夾，資料結構可參考 [SASS guideline](https://sass-guidelin.es/#architecture)


**js**

主程式為 app.js，可使用 vue，vue 的元件可整理在 components


**resources**

fonts 放字形檔、images 放圖片；icons 為製作 sprites icon 用，產出時不會出現在 dist


**views**

- **layouts** 放置共用模板
- **pages** 主要切版產出的頁面
- **partials** 放置共用元組件



## 指令

### gulp

產出 HTML / JS / CSS 到 dist folder

### gulp watch

產出 HTML / JS / CSS 到 dist folder，並呼叫 browserSync，可在開發時做即時監控

### gulp minify

把 JS / CSS / IMG 做壓縮

### gulp archive

打包 dist 到  archive/project-name_20190101.zip


### package.json
```
"f2e-configs": {
    "scripts": [  // 整合外部 JS 為 lib.js
        "./node_modules/jquery/dist/jquery.js",
        "./node_modules/bootstrap/dist/js/bootstrap.js"
    ],
    "styles": [   // 整合外部 CSS 為 lib.css
    "./node_modules/bootstrap/dist/css/bootstrap.css"
    ],
    "fonts": []  // 載入外部字型到 fonts
},
```